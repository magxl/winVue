const path = require("path")
const { env } = require("process")
console.info("当前模块化名称：", env.NODE_ENV)
module.exports = {
  publicPath: "/",
  devServer: {
    hot: true,
    https: true,
    port: 8812,
  },
  productionSourceMap: true,
  configureWebpack: {
    resolve: {
      alias: {
        js: path.join(__dirname, "src/js"),
        map: path.join(__dirname, "src/js/map"),
        cpt: path.join(__dirname, "src/components"),
      },
    },
  },
  css: {
    loaderOptions: {
      scss: {
        //全局变量预设 sassloader8- 用 data , 10+ 用 additionalData
        prependData: `@import "src/assets/css/main.scss";`,
      },
    },
  },
  lintOnSave: true,
}
