// 合并单色图标和多色图标，此处多色弃用
export default {
  functional: true,
  name: "ColorfulIcon",
  props: {
    name: {
      type: String,
      default: "",
    },
  },
  render(h, ctx) {
    return h(
      "svg",
      {
        attrs: {
          class: "ColorfulIcon",
          ariaHidden: true,
        },
      },
      [
        h("use", {
          attrs: {
            "xlink:href": "#win-" + (ctx.props.name || "unknow"),
          },
        }),
      ]
    )
  },
}
