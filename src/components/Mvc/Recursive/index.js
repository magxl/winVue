/* 
  定义slot-> name="level"+index
    以level下标来处理 不同层级不同插槽 的需求
  定义slot-> default
    以默认插槽来处理无需定义的插槽
  传入levelClass 来设置每一层的公共样式，默认插槽时可以使用此prop来定义层级偏移缩进等
  dt 组件数据，必须是可递归的数组
*/
export default {
  functional: true,
  props: {
    dt: {
      type: Array,
      default: () => [],
    },
    levelClass: {
      type: String,
      default: "",
    },
    children: {
      type: String,
      default: "children",
    },
  },
  render: function(h, c) {
    let slots = c.scopedSlots,
      levelClass = c.props.levelClass,
      children = c.props.children
    function recursive(dt, index = 0) {
      let levelIndex = index || 0
      levelIndex++
      return dt.map((it, i) => {
        const level = "level" + levelIndex,
          slot = slots[level]
            ? slots[level]({ it, i })
            : slots["default"]({ it, i })
        if (it[children]) {
          return [
            h("div", { class: levelClass + " " + level }, [
              slot,
              h(
                "div",
                { class: level + "Children" },
                recursive(it[children], levelIndex)
              ),
            ]),
          ]
        } else {
          return h("div", { class: levelClass + " " + level }, [slot])
        }
      })
    }
    let dt = c.props.dt
    if (dt) {
      return h(
        "div",
        {
          inheritAttrs: false,
          class: "CPT_Recursive",
        },
        recursive(dt)
      )
    }
  },
}
