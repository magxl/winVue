import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import desk from "./desk"

const allStore = {
  desk
}

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: allStore,
});
