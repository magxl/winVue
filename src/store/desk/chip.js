export default {
  async getCurrentWindow({ state }, v) {
    let windows
    if (v) {
      windows = v
    } else {
      windows = state.windows
    }
    return windows.filter(it => it.attrs.active === true)[0]
  },
  async unactiveCurrent({ dispatch }, windows) {
    // 让上一次激活的窗口 失活
    const oacwin = await dispatch("getCurrentWindow", windows)
    if (oacwin) {
      const oi = windows.indexOf(oacwin)
      windows[oi].attrs.active = false
    }
    return windows
  },
  async activeCurrent({ state, commit }, info) {
    //激活窗口
    let z = state.z + 1
    info.attrs = info.attrs || {}
    info.attrs.z = z
    info.attrs.active = true
    commit("saveData", { aim: "z", dt: z })
    return info
  },
}
