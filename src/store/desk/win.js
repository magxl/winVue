export default {
  getWindow({ state }, app) {
    //传入router数据，获取窗口索引
    let windows = state.windows,
      i = windows.indexOf(windows.filter(it => it.name === app.name)[0]),
      windowInfo = { i }
    windowInfo.info = windows[i]
    windowInfo.windows = window.Vue.$deepCopy(windows)
    return windowInfo
  },
  initWindow({ state }, { attrs: dattrs = {} }) {
    let { attrs } = state,
      id = "win" + +new Date() //唯一标识
    return { ...attrs, ...dattrs, id }
  },
  async openWindow({ state, commit, dispatch }, app) {
    //打开窗口
    let { i, info, windows } = await dispatch("getWindow", app)
    if (i > -1) {
      //要打开的窗口已打开
      if (!info.attrs.active) {
        // 未激活
        if (info.attrs.status === "mini") {
          dispatch("toActive", { app: info, status: info.attrs.ostatus })
        }
        info = await dispatch("activeCurrent", info)
        app = info
      }
    } else {
      document.body.style.cursor = "progress"
      let attrs = await dispatch("initWindow", app)
      app.attrs = attrs
      app = await dispatch("activeCurrent", app)
      //当前激活的窗口是cwindow
      const cwindow = await dispatch("getCurrentWindow", windows)
      windows = await dispatch("unactiveCurrent", windows)
      //初始化坐标
      const { offset, diysize } = state.config
      if (cwindow) {
        //新打开的窗口对于当前激活窗口的坐标偏移
        const cwStatus = cwindow.attrs.status
        let { left = "0px", top = "0px" } = cwindow.attrs[cwStatus]
        left = parseInt(left) || 0
        top = parseInt(top) || 0
        app.attrs.diy.left = left + offset[0] + "px"
        app.attrs.diy.top = top + offset[0] + "px"
      } else {
        //无激活窗口时，默认居中位置
        app.attrs.diy.left =
          parseInt(document.body.clientWidth - diysize[0]) / 2 + "px"
        app.attrs.diy.top =
          parseInt(document.body.clientHeight - diysize[1]) / 2 + "px"
      }

      const appCopy = window.Vue.$deepCopy(app)
      app.nav = [appCopy] //备份当前层级信息 给到其文件夹导航
      windows.push(app)
      commit("saveWindows", windows)
      //打开窗口结束
      let timer = window.Vue.$setTimeout(
        () => {
          document.body.removeAttribute("style")
          commit("saveData", { aim: "deskActiveApp", dt: "" }) //取消鼠标选中的app效果
        },
        500,
        timer
      )
    }
  },
  async changeWindow({ commit, dispatch }, { app, status }) {
    //改变窗口状态
    let { i, info, windows } = await dispatch("getWindow", app)
    //更新新旧状态
    const ostatus = info.attrs.status
    info.attrs.status = status
    info.attrs.ostatus = ostatus
    if (status !== "mini") {
      //非最小，且未激活，去激活
      if (!info.attrs.active) {
        //失活上一个激活
        windows = await dispatch("unactiveCurrent", windows)
        //激活当前这个
        //计算zindex
        info = await dispatch("activeCurrent", info)
      }
    } else {
      //最小，若激活，则失活
      if (info.attrs.active) {
        info.attrs.active = false
      }
    }
    windows[i].attrs = info.attrs
    commit("saveWindows", windows)
  },
  async fullWindow({ commit, dispatch }, app) {
    let { i, info, windows } = await dispatch("getWindow", app)
    if (!info.attrs.active) {
      //失活上一个激活
      windows = await dispatch("unactiveCurrent", windows)
      //激活当前这个
      //计算zindex
      info = await dispatch("activeCurrent", info)
    }
    info.attrs.ostatus = "diy"
    info.attrs.status = "full"
    windows[i].attrs = info.attrs
    commit("saveWindows", windows)
  },
  async diyWindow({ commit, dispatch }, app) {
    let { i, info, windows } = await dispatch("getWindow", app)
    if (!info.attrs.active) {
      //失活上一个激活
      windows = await dispatch("unactiveCurrent", windows)
      //激活当前这个
      //计算zindex
      info = await dispatch("activeCurrent", info)
    }
    info.attrs.ostatus = "full"
    info.attrs.status = "diy"
    windows[i].attrs = info.attrs
    commit("saveWindows", windows)
  },
  async hideWindow({ commit, dispatch }, app) {
    let { i, info, windows } = await dispatch("getWindow", app)
    info.attrs.ostatus = info.attrs.status
    info.attrs.status = "mini"
    info.attrs.active = false
    windows[i].attrs = info.attrs
    commit("saveWindows", windows)
  },
  async showWindow({ commit, dispatch }, app) {
    let { i, info, windows } = await dispatch("getWindow", app)
    windows = await dispatch("unactiveCurrent", windows)
    info.attrs.status = info.attrs.ostatus
    info = await dispatch("activeCurrent", info)
    windows[i].attrs = info.attrs
    commit("saveWindows", windows)
  },
  async activeWindow({ commit, dispatch }, app) {
    //激活窗口
    if (app.attrs.active) {
      return
    }
    let { i, info, windows } = await dispatch("getWindow", app)
    //失活上一个窗口
    windows = await dispatch("unactiveCurrent", windows)
    //激活当前窗口
    info = await dispatch("activeCurrent", info)
    windows[i].attrs = info.attrs
    commit("saveWindows", windows)
  },
  async moveWindow({ commit, dispatch }, { app, style }) {
    // 移动窗口
    let { i, info, windows } = await dispatch("getWindow", app)
    const { status } = info.attrs,
      offset = {
        left: style.left,
        top: style.top,
      }
    info.attrs[status] = offset
    windows[i] = info
    commit("saveWindows", windows)
  },
  async closeWindow({ commit, dispatch }, app) {
    //关闭窗口
    let { i, windows } = await dispatch("getWindow", app)
    windows.splice(i, 1)
    commit("saveWindows", windows)
  },
  async changeViewMode({ commit, dispatch }, { app, it }) {
    let { i, windows } = await dispatch("getWindow", app)
    windows[i].attrs.viewMode = it.icon
    commit("saveWindows", windows)
  },
}
