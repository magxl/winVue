export default {
  saveData(s, { aim, dt }) {
    if (window.Vue.$getType(dt) === "Object") {
      s[aim] = Object.assign({}, dt)
    } else if (window.Vue.$getType(dt) === "Array") {
      s[aim] = [].concat(dt)
    } else {
      s[aim] = dt
    }
  },
  saveRmenu(s, dt) {
    if (dt) {
      let { btn, aim, width, top, left } = dt
      if (aim) {
        let rmenu = { btn, top: aim.clientY, left: aim.clientX }
        if (width) {
          rmenu.width = width
        } else {
          rmenu.width = s.rmenu.width
        }
        s.rmenu = rmenu
      } else {
        //更新坐标
        s.rmenu = {
          ...s.rmenu,
          top,
          left,
        }
      }
    } else {
      s.rmenu = dt
    }
  },
  saveWindows(s, dt) {
    s.windows = dt
  },
  toggleWinPanel(s) {
    s.winPanel = !s.winPanel
  },
}
