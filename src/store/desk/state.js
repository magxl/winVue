export default {
  loading: {
    visible: false,
    stamp: 0,
    preventStop: false, //true载入时禁止点击，false不禁止
  },
  rmenu: false, //右键菜单数据
  acwindow: 0, //当前激活的窗口 id
  z: 0, //最高层级
  windows: [], //窗口数据 每一个主路由是一个窗口，此处存储的是打开的主路由页面
  openloading: false, //窗口打开时 loading状态
  stamp: 0, //节流时间戳
  config: {
    offset: [16, 16], //窗口打开后偏移量
    diysize: [800, 600], //初始窗口大小
  },
  attrs: {
    status: "diy", //默认窗口模式 小窗口 diy|full|mini
    ostatus: "diy",
    diy: {
      left: 0,
      top: 0,
    },
    full: {
      left: 0,
      top: 0,
    },
    mini: {
      left: "50%",
      top: "calc(100vh - 3rem)",
    },
    viewMode: "list", //默认视图模式 列表 list|thumb
    active: true, //打开时默认激活到最前
  },
  winPanel: false, //win面板激活
  tabbar: {
    hoverViewTime: 200, //鼠标进入 伪激活窗口的延迟间隔，即200毫秒后显示藏在后面的窗口
  },
  deskActiveApp: "", //鼠标选中的app
}
