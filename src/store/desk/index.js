import state from "./state.js"
import mutations from "./mutations.js"
import win from "./win.js"
import chip from "./chip.js"
const actions = {
  ...win,
  ...chip,
  async changeNav({ state, commit, dispatch }, { app, i: index }) {
    //改变页内面包屑导航
    const { i, windows } = await dispatch("getWindow", app)
    let nav = windows[i].nav
    nav = nav.splice(0, index + 1)
    windows[i].nav = nav
    commit("saveWindows", windows)
  },
  async toChangeViewMode({ state, commit, dispatch }, { app, it }) {
    // 改变窗口视图类型 缩略|列表
    const { i, windows } = await dispatch("getWindow", app)
    windows[i].attrs.viewMode = it.icon;
    commit("saveWindows", windows)
  },
}
const getters = {}
export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
}
