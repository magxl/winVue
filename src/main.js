import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"
import "./assets/css/theme.scss"
import plugin from "js/plugin"
Vue.use(plugin)
Vue.config.productionTip = false

window.Vue = new Vue({
  router,
  store,
  render: h => h(App),
}).$mount("#WINDOWS12")
