import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter)
const routes = [
  {
    path: "/",
    name: "Main",
    component: () => import("../views/Main/Main"),
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});
export default router;
