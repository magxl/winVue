export default {
  mp3: {
    type: "audio/mpeg",
    code: [73, 68, 51, 4],
  },
  wav: {
    type: "audio/wav",
    code: [82, 73, 70, 70],
  },
  xls: {
    type: "application/vnd.ms-excel",
    code: [208, 207, 17, 224],
  },
  xlsx: {
    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    code: [80, 75, 3, 4],
  },
  txt: {
    type: "text/plain",
    code: [49, 53, 49, 49],
  },
  csv: {
    type: "application/vnd.ms-excel",
    code: [105, 100, 44, 99],
  },
  pdf: {
    type: "application/pdf",
    code: [37, 80, 68, 70],
  },
}
