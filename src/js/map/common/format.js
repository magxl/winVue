//文件格式字典
export default {
  folder: "文件夹",
  video: "视频",
  audio: "音频",
  web: "网页",
  ppt: "幻灯片",
  word: "图文文档",
  pdf: "可携带文档",
  xls: "电子表格",
  txt: "文本文档",
  image: "图片",
  zip: "压缩文件",
  link: "超链接",
  code: "代码片段",
  question: "问答",
  calendar: "日历",
  unknow: "未知", //一切其它格式 都认为是未知
}
