export default {
  onlythree:[
    {value:'110',label:'报警'},
    {value:'114',label:'查号台'},
    {value:'119',label:'火警'},
    {value:'120',label:'急救'},
    {value:'122',label:'交警'},
    {value:'124',label:'寻呼'},
    {value:'999',label:'报警'},
  ],
  three: [
    { value: "134", label: "移动" },
    { value: "135", label: "移动" },
    { value: "136", label: "移动" },
    { value: "137", label: "移动" },
    { value: "138", label: "移动" },
    { value: "139", label: "移动" },
    { value: "147", label: "移动" },
    { value: "148", label: "移动" },
    { value: "150", label: "移动" },
    { value: "151", label: "移动" },
    { value: "152", label: "移动" },
    { value: "157", label: "移动" },
    { value: "158", label: "移动" },
    { value: "159", label: "移动" },
    { value: "172", label: "移动" },
    { value: "178", label: "移动" },
    { value: "182", label: "移动" },
    { value: "183", label: "移动" },
    { value: "184", label: "移动" },
    { value: "187", label: "移动" },
    { value: "188", label: "移动" },
    { value: "195", label: "移动" },
    { value: "197", label: "移动" },
    { value: "198", label: "移动" },
    { value: "130", label: "联通" },
    { value: "131", label: "联通" },
    { value: "132", label: "联通" },
    { value: "145", label: "联通" },
    { value: "155", label: "联通" },
    { value: "156", label: "联通" },
    { value: "166", label: "联通" },
    { value: "167", label: "联通" },
    { value: "171", label: "联通" },
    { value: "175", label: "联通" },
    { value: "176", label: "联通" },
    { value: "185", label: "联通" },
    { value: "186", label: "联通" },
    { value: "196", label: "联通" },
    { value: "133", label: "电信" },
    { value: "149", label: "电信" },
    { value: "153", label: "电信" },
    { value: "173", label: "电信" },
    { value: "177", label: "电信" },
    { value: "180", label: "电信" },
    { value: "181", label: "电信" },
    { value: "189", label: "电信" },
    { value: "190", label: "电信" },
    { value: "191", label: "电信" },
    { value: "193", label: "电信" },
    { value: "199", label: "电信" },
    { value: "162", label: "虚拟电信" },
    { value: "165", label: "虚拟移动" },
    { value: "171", label: "虚拟联通" },
    { value: "167", label: "虚拟联通" },
    { value: "174", label: "卫星" },
    { value: "140", label: "物联网" },
    { value: "141", label: "物联网" },
    { value: "144", label: "物联网" },
    { value: "146", label: "物联网" },
    { value: "148", label: "物联网" },
  ],
  four: [
    { value: "1440", label: "移动" },
    { value: "1700", label: "虚拟电信" },
    { value: "1701", label: "虚拟电信" },
    { value: "1702", label: "虚拟电信" },
    { value: "1703", label: "虚拟移动" },
    { value: "1705", label: "虚拟移动" },
    { value: "1706", label: "虚拟移动" },
    { value: "1704", label: "虚拟联通" },
    { value: "1707", label: "虚拟联通" },
    { value: "1708", label: "虚拟联通" },
    { value: "1709", label: "虚拟联通" },
    { value: "1349", label: "卫星" },
  ],
  five:[
    { value: "10000", label: "电信客服" },
    { value: "10010", label: "电信客服" },
    { value: "10050", label: "铁通客服" },
    { value: "10060", label: "网通客服" },
    { value: "10086", label: "联通客服" },
    { value: "12121", label: "天气预报" },
    { value: "12306", label: "铁路订票" },
    { value: "12117", label: "报时台" },
    { value: "11185", label: "邮政专递" },
    { value: "12345", label: "政府服务" },
    { value: "12348", label: "法律援助" },
  ]
};