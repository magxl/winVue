import keycodeMap from "./keycodeMap"
import phoneServiceMap from "./phoneServiceMap"
import mimeTypeMap from "./mimeTypeMap"
import format from "./format"

export default {
  keycodeMap,
  phoneServiceMap,
  mimeTypeMap,
  format
};