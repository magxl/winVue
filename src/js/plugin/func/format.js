import { js, css, html } from "js-beautify"
export default Vue => {
  Vue.prototype.$format = function(dt, format) {
    //
    if (["js", "javascript", "script"].indexOf(format) > -1) {
      return js(dt, { indent_size: 2 })
    } else if (["html", ""].indexOf(format) > -1) {
      return html(dt, { indent_size: 2 })
    } else {
      return css(dt)
    }
  }
}
