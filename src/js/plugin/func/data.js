export default Vue => {
  Vue.prototype.$key2Array = function (v) {
    //key:label类数组转[{label:"",value:""}]数组
    let r = []
    for (let k in v) {
      r.push({
        label: v[k],
        value: k,
      })
    }
    return r
  }
  Vue.prototype.$ab2url = function (arrayBuffer, type = "audio/wave") {
    const blob = new Blob([arrayBuffer], { type })
    const url = URL.createObjectURL(blob)
    return url
  }
  Vue.prototype.$blob2url = function (blob) {
    console.info(blob)
    return URL.createObjectURL(blob)
  }
  Vue.prototype.$typeData = function (dt) {
    //判断类型
    return Object.prototype.toString.call(dt).slice(8, -1)
  }
  Vue.prototype.$getType = function (dt) {
    //判断类型
    return Object.prototype.toString.call(dt).slice(8, -1)
  }
  Vue.prototype.$deepCopy = function (dt) {
    //递归深拷贝
    let type = Vue.prototype.$getType(dt)
    let r = null
    if (type === "Object") {
      r = {}
    } else if (type === "Array") {
      r = []
    } else {
      return dt
    }
    for (let k in dt) {
      if (Object.prototype.hasOwnProperty.call(dt, k)) {
        const type = Vue.prototype.$getType(dt[k])
        if (type === "Object" || type === "Array") {
          r[k] = this.$deepCopy(dt[k])
        } else {
          r[k] = dt[k]
        }
      }
    }
    return r
  }
  Vue.prototype.$delVueObj = function (dt) {
    return JSON.parse(JSON.stringify(dt))
  }
  Vue.prototype.$delEmptyData = function (dt) {
    //深度过滤空参数
    let type = Vue.prototype.$typeData(dt)
    let empty = ["", null, undefined]
    let r = {}
    try {
      if (type === "Object") {
        for (let k in dt) {
          if (Object.prototype.hasOwnProperty.call(dt, k)) {
            let ttype = Vue.prototype.$typeData(dt[k])
            if (ttype === "Object") {
              r[k] = Vue.prototype.$delEmptyData(dt[k])
            } else if (type === "Array") {
              //空数组
              if (dt[k].length) {
                r[k] = dt[k]
              }
            } else {
              if (!empty.includes(dt[k])) {
                r[k] = dt[k]
              }
            }
          }
        }
      } else if (type === "Array") {
        if (dt.length) {
          return dt
        }
      } else {
        if (!empty.includes(dt)) {
          return dt
        }
      }
      return r
    } catch (error) {
      console.info("传入一个Object")
    }
  }
  Vue.prototype.$randomNumber = function (dt, min = 0) {
    //随机整数
    let r = parseInt(Math.random() * dt)
    if (min) {
      r = r < min ? r + min : r
    }
    return r
  }
}
