import { common } from "js/map"
const { keycodeMap } = common
export default Vue => {
  Vue.prototype.$copy = function(dt) {
    //拷贝到剪切板
    let url = dt
    let oInput = document.createElement("input")
    oInput.value = url
    document.body.appendChild(oInput)
    oInput.select()
    document.execCommand("Copy") // 无可替代更新
    Vue.prototype.$message.success({ message: "已复制 " + dt, center: true })
    oInput.remove()
  }
  Vue.prototype.$resetData = function() {
    //重置实例data
    Object.assign(this.$data, this.$options.data.call(this))
  }
  Vue.prototype.$setInterval = function(cb = () => {}, time, otimer) {
    //自动卸载Interval
    let timer = null
    if (otimer) {
      clearInterval(otimer)
      otimer = null
    }
    timer = setInterval(cb, time)
    this.$once("hook:beforeDestroy", () => {
      clearInterval(timer)
      timer = null
    })
    return timer
  }
  Vue.prototype.$setTimeout = function(cb = () => {}, time, otimer) {
    //自动卸载Timeout
    let timer = null
    if (otimer) {
      clearInterval(otimer)
      otimer = null
    }
    timer = setTimeout(cb, time)
    this.$once("hook:beforeDestroy", () => {
      clearTimeout(timer)
      timer = null
    })
    return timer
  }
  Vue.prototype.$keydown = function(v) {
    //自动卸载keydown||||||created/mounted内使用
    //参数 Array
    /* 
      {
        keyname:"", //字母,
        keycode:0, //ascii码
        ctrl: true, //默认需要按住ctrl
        alt: true, //默认需要按住alt
        shift: true, //默认不需要shift
        cb(){}, //回调
      }
    */
    let that = this
    that.diykeys = v
    //初始化自定义按键
    const okeys = window?.diykeys?.keys || {}
    //格式化按键组
    v.map(it => {
      let k = it.keycode
      if(it.keyname){
        k = it.keyname.toUpperCase()
        it._keyname = k
      }
      okeys[k] = it
    })
    window.diykeys = {
      cas: {
        alt: false,
        ctrl: false,
        shift: false,
      },
      keys: okeys,
    }
    let keys = window.diykeys.keys
    //有传入key帧听
    document.onkeydown = function(e) {
      let keycode = e.keyCode || e.which || e.charCode
      let keyname = e.key.toUpperCase()
      if (keycode === 18) {
        window.diykeys.cas.alt = true
        return
      }
      if (keycode === 17) {
        window.diykeys.cas.ctrl = true
        return
      }
      if (keycode === 16) {
        window.diykeys.cas.shift = true
        return
      }
      //寻找按键事件
      const key = keys[e] || keys[keyname]
      if (key) {
        //存在按键帧听，匹配 ctrl alt shift 状态
        const alt = key.alt !== undefined ? key.alt : true
        const ctrl = key.ctrl !== undefined ? key.ctrl : true
        const shift = key.shift !== undefined ? key.shift : false
        if (
          window.diykeys.cas.ctrl === ctrl &&
          window.diykeys.cas.alt === alt &&
          window.diykeys.cas.shift === shift
        ) {
          //状态满足 执行回调
          key.cb()
        }
      }
    }
    document.onkeyup = function(e) {
      //按键回弹 重置 cas 状态
      let keycode = e.keyCode || e.which || e.charCode
      if (keycode === 18) {
        window.diykeys.cas.alt = false
      }
      if (keycode === 17) {
        window.diykeys.cas.ctrl = false
      }
      if (keycode === 16) {
        window.diykeys.cas.shift = false
      }
    }
    that.$once("hook:beforeDestroy", () => {
      if (that.diykeys) {
        that.diykeys.map(it=>{
          let key = it._keyname||it.keycode
          delete window.diykeys.keys[key]
        })
        if (!Object.keys(window.diykeys.keys).length) {
          document.onkeydown = null
          document.onkeyup = null
        }
      }
    })
  }
}
