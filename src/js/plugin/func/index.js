//全局挂vue方法
import normal from "./normal" //引用第三方|项目内必备常规封装
import media from "./media" //媒体类 音视频图文
import data from "./data" //数据类 各类转换处理
import date from "./date" //日期类 格式化 计算
import extra from "./extra" //封装便捷 DIY便捷功能
import notice from "./notice" //全局消息提示 进行节流
import windows from "./windows" //窗口快捷指令
import format from "./format" //格式化
export default Vue=>{
  // normal(Vue)
  media(Vue)
  data(Vue)
  date(Vue)
  extra(Vue)
  notice(Vue)
  windows(Vue)
  normal(Vue)
  format(Vue)
};