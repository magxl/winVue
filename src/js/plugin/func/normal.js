// import * as rq from "rq"
// import api from "api"
import app from "/src/apps"
export default Vue => {
  //图表
  // Vue.prototype.$echarts = echarts
  //接口文档
  // Vue.prototype.$api = api
  //应用
  Vue.prototype.$apps = app
  //请求
  // Vue.prototype.$get = rq.get
  // Vue.prototype.$post = rq.post
  // Vue.prototype.$put = rq.put
  // Vue.prototype.$del = rq.del
  // Vue.prototype.$update = rq.update
  // Vue.prototype.$form = rq.form
}
