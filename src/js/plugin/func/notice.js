export default Vue => {
  //全局提示消息
  Vue.prototype.$notice = v => {
    const type = Vue.prototype.$getType(v)
    if (type === "String") {
      v = {
        message: v,
        duration: 3000,
      }
    }
    v.onClose = (e)=>{
      Vue.prototype.$store.commit("launch/removeNotice", e)
    }
    Vue.prototype.$store.dispatch("launch/addNotice", v)
  }

  function warning(v) {
    const type = Vue.prototype.$getType(v)
    if (type === "String") {
      v = {
        message: v,
      }
    } else if (type === "Object") {
      if (v.type) {
        delete v.type
      }
      if (v.iconClass) {
        delete v.iconClass
      }
      if (v.title) {
        delete v.title
      }
    }
    v.onClose = (e)=>{
      Vue.prototype.$store.commit("launch/removeNotice", e)
    }
    Vue.prototype.$store.dispatch("launch/addNotice", {
      title: "警告",
      duration: 3000,
      iconClass: "el-icon-warning-outline txt-orange",
      ...v,
    })
  }
  function success(v) {
    const type = Vue.prototype.$getType(v)
    if (type === "String") {
      v = {
        message: v,
      }
    } else if (type === "Object") {
      if (v.type) {
        delete v.type
      }
      if (v.iconClass) {
        delete v.iconClass
      }
      if (v.title) {
        delete v.title
      }
    }
    v.onClose = (e)=>{
      Vue.prototype.$store.commit("launch/removeNotice", e)
    }
    Vue.prototype.$store.dispatch("launch/addNotice", {
      title: "成功",
      duration: 3000,
      iconClass: "el-icon-circle-check txt-success",
      ...v,
    })
  }
  function error(v) {
    const type = Vue.prototype.$getType(v)
    if (type === "String") {
      v = {
        message: v,
      }
    } else if (type === "Object") {
      if (v.type) {
        delete v.type
      }
      if (v.iconClass) {
        delete v.iconClass
      }
      if (v.title) {
        delete v.title
      }
    }
    v.onClose = (e)=>{
      Vue.prototype.$store.commit("launch/removeNotice", e)
    }
    Vue.prototype.$store.dispatch("launch/addNotice", {
      title: "失败",
      duration: 3000,
      iconClass: "el-icon-circle-close txt-red",
      ...v,
    })
  }
  function info(v) {
    const type = Vue.prototype.$getType(v)
    if (type === "String") {
      v = {
        message: v,
      }
    } else if (type === "Object") {
      if (v.type) {
        delete v.type
      }
      if (v.iconClass) {
        delete v.iconClass
      }
      if (v.title) {
        delete v.title
      }
    }
    v.onClose = (e)=>{
      Vue.prototype.$store.commit("launch/removeNotice", e)
    }
    Vue.prototype.$store.dispatch("launch/addNotice", {
      title: "提示",
      duration: 2000,
      iconClass: "el-icon-info txt-blue",
      ...v,
    })
  }
  Vue.prototype.$notice.__proto__.warning = warning
  Vue.prototype.$notice.__proto__.success = success
  Vue.prototype.$notice.__proto__.error = error
  Vue.prototype.$notice.__proto__.info = info
}
