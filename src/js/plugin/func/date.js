export default Vue => {
  Vue.prototype.$getFullDate = function (dt) {
    //转换为YYYY-MM-DD格式的日期
    if (Vue.prototype.$getType(dt) === "Number") {
      dt = new Date(dt)
    }
    let Y = dt.getFullYear(),
      M = dt.getMonth() + 1,
      D = dt.getDate()
    M = M > 9 ? M : "0" + M
    D = D > 9 ? D : "0" + D
    const r = Y + "-" + M + "-" + D
    return r
  }
  Vue.prototype.$getFullDateTime = function (dt, type) {
    //转换为YYYY-MM-DD hh:mm:ss tttt格式的日期
    if (Vue.prototype.$getType(dt) === "Number") {
      dt = new Date(dt)
    }
    let Y = dt.getFullYear(),
      M = dt.getMonth() + 1,
      D = dt.getDate(),
      h = dt.getHours(),
      m = dt.getMinutes(),
      s = dt.getSeconds(),
      t = dt.getMilliseconds()
    M = M > 9 ? M : "0" + M
    D = D > 9 ? D : "0" + D
    h = h > 9 ? h : "0" + h
    m = m > 9 ? m : "0" + m
    s = s > 9 ? s : "0" + s
    let r = Y + "-" + M + "-" + D + " " + h + ":" + m + ":" + s + " " + t
    if (type === "hh:mm") {
      r = h + ":" + m
    } else if (type === "date") {
      r = Y + "-" + M + "-" + D
    } else if (type === "time") {
      r = h + ":" + m + ":" + s
    }
    return r
  }
  Vue.prototype.$formatTime = function (dt) {
    //转换秒数为 分:秒
    if (dt !== undefined) {
      let m = parseInt(dt / 60),
        s = dt % 60
      m = m > 9 ? m : "0" + m
      s = s > 9 ? s : "0" + s

      return m + ":" + s
    } else {
      console.error("需要传入number数据")
    }
  }
}
