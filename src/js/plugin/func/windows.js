export default Vue => {
  Vue.prototype.$getApp = function (v) {
    //v 为 route的name
    const route = Vue.prototype.$apps.filter(it => it.name === v)[0]
    if (route) {
      return route
    } else {
      console.error("未找到应用信息", v.toString())
      return false
    }
  }
}
