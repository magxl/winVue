import Apps from "cpt/Mvc/Apps"
import Desk from "cpt/Mvc/Desk"
import Icon from "cpt/Mvc/Icon"
import Page from "cpt/Mvc/Page"
import Rmenu from "cpt/Mvc/Rmenu"
import Tabbar from "cpt/Mvc/Tabbar"
import Layout from "cpt/Mvc/Layout"
import Windows from "cpt/Mvc/Windows"
import CodeArea from "cpt/Mvc/CodeArea"
import WinPanel from "cpt/Mvc/WinPanel"
import Recursive from "cpt/Mvc/Recursive"
import RmenuItem from "cpt/Mvc/Rmenu/chip/RmenuItem.vue"
import Scrollbar from "cpt/Mvc/Scrollbar"
// ↓ plugin
import Clock from "cpt/Plugin/Clock"
import Time from "cpt/Plugin/Time"
import Task from "cpt/Plugin/Task"
import BorderFactory from "cpt/Plugin/BorderFactory"
import BorderFactoryItem from "cpt/Plugin/BorderFactory/chip/BorderFactoryItem"
import Island from "cpt/Plugin/Island"

export default Vue => {
  Vue.component("Apps", Apps)
  Vue.component("Desk", Desk)
  Vue.component("Icon", Icon)
  Vue.component("Page", Page)
  Vue.component("Rmenu", Rmenu)
  Vue.component("Tabbar", Tabbar)
  Vue.component("Layout", Layout)
  Vue.component("Windows", Windows)
  Vue.component("CodeArea", CodeArea)
  Vue.component("WinPanel", WinPanel)
  Vue.component("Recursive", Recursive)
  Vue.component("RmenuItem", RmenuItem)
  Vue.component("Scrollbar", Scrollbar)

  Vue.component("Clock", Clock)
  Vue.component("Time", Time)
  Vue.component("Task", Task)
  Vue.component("Island", Island)
  Vue.component("BorderFactory", BorderFactory)
  Vue.component("BorderFactoryItem", BorderFactoryItem)

}
