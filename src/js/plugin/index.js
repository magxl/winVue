import cpt from "./cpt"
import func from "./func/"
import element from "./element"
import directive from "./directive/"
export default Vue => {
  cpt(Vue)
  func(Vue)
  element(Vue)
  directive(Vue)
}
