import { Scrollbar } from "element-ui"
export default Vue => {
  Vue.component("ElScrollbar", Scrollbar)
}
