export default Vue => {
  //访问权限
  Vue.directive("access", {
    inserted(el, binding) {
      const { value } = binding
      const roles = ["权限组"] //这里是已授权的权限，供比对
      if (value?.length && Vue.prototype.$getType(value) === "Array") {
        const hasPermission = roles.filter(it => {
          return value.indexOf(it) > -1
        })
        if (!hasPermission) {
          el.parentNode.removeChild(el)
        }
      } else {
        console.error("v-access='['access1','access2']'")
      }
    },
  })
}
