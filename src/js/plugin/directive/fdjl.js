export default Vue => {
  let timer = null
  //防抖 v-fd:click / v-fd:input ...
  // = {time:300, cb(){...}} 填充参数
  Vue.directive("fd", {
    bind: (el, binding) => {
      const { value, arg } = binding
      let cb,
        time = 300
      if (Vue.prototype.$getType(value) === "Function") {
        cb = value
      } else {
        cb = value.cb
        time = value.time
      }
      let cmd = arg || "click"
      cmd = "on" + cmd
      el[cmd] = function () {
        clearTimeout(timer)
        timer = setTimeout(() => {
          cb()
        }, time)
      }
    },
  })
  //节流 v-jl:click / v-jl:input ...
  // = {time:300, cb(){...}} 填充参数
  Vue.directive("jl", {
    bind: (el, binding) => {
      const { value, arg } = binding
      let cb,
        time = 300
      if (Vue.prototype.$getType(value) === "Function") {
        cb = value
      } else {
        cb = value.cb
        time = value.time
      }
      let stamp = null
      let cmd = arg || "click"
      cmd = "on" + cmd
      el[cmd] = function () {
        let now = new Date().getTime()
        if (stamp) {
          if (now - stamp > time) {
            cb()
          }
        } else {
          cb()
        }
        stamp = now
      }
    },
  })
}
