import drag from "./drag.js"
import fdjl from "./fdjl.js"
import access from "./access.js"
import highlight from "./highlight.js"
export default Vue => {
  drag(Vue)
  fdjl(Vue)
  access(Vue)
  highlight(Vue)
}
