//拖动
export default Vue => {
  const drag = Vue.directive("drag", {
    inserted: (el, binding) => {
      let { value = {} } = binding
      let { dragMode, ref, status, cb = () => {} } = value
      let dragAim = el
      let checkDragArea = child => {
        let { localName, dataset = {} } = child
        if (dataset.moveaim !== undefined) {
          dragAim = child
        } else {
          if (localName !== "body") {
            checkDragArea(child.parentNode)
          }
        }
      }
      checkDragArea(el)
      // timer = setTimeout(() => {
      //   checkDragArea(el)
      // })
      el.onmousedown = function (e) {
        if (status === "full") {
          //全屏模式不可移动
          return
        }
        el.setAttribute("data-moving", "moving")
        dragAim.setAttribute("data-moveaim", "moving")
        let dcw = document.body.offsetWidth
        let dch = document.body.offsetHeight
        let left, top
        let x = e.pageX - dragAim.offsetLeft
        let y = e.pageY - dragAim.offsetTop
        document.onmousemove = function (e) {
          if (dragMode === "v") {
            left = dcw - dragAim.width
            top = e.pageY - y
          } else if (dragMode === "h") {
            left = e.pageX - x
            top = dch - dragAim.height
          } else {
            left = e.pageX - x
            top = e.pageY - y
          }
          dragAim.style.left = left + "px"
          dragAim.style.top = top + "px"
          dragAim.style.right = "auto"
          dragAim.style.bottom = "auto"
        }
        document.onmouseup = function () {
          cb(dragAim.style)
          el.setAttribute("data-moving", "")
          dragAim.setAttribute("data-moveaim", "")
          // et = new Date().getTime()
          // if (et - st < sep) {
          //   el.setAttribute("data-moving", "")
          // }
          document.onmousemove = document.onmouseup = null
          // clearTimeout(timer)
        }
      }
    },
    update: (el, binding) => {
      let { value, oldValue } = binding
      if (value.toString() !== oldValue.toString()) {
        drag.inserted(el, binding)
      }
    },
    unbind: () => {},
  })
}
