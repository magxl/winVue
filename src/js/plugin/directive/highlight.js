const highlight = () =>
  import(/* webpackChunkName: "highlight" */ "highlight.js")
import "highlight.js/styles/tomorrow-night-blue.css"
export default Vue => {
  let Hljs
  //高亮代码块
  Vue.directive("highlight", {
    // 被绑定元素插入父节点时调用
    inserted: async function(el) {
      let blocks = el.querySelectorAll("pre code")
      for (let i = 0; i < blocks.length; i++) {
        if(!Hljs){
          Hljs = await highlight()
        }
        Hljs.highlightElement(blocks[i])
      }
    },
    // 指令所在组件的 VNode 及其子 VNode 全部更新后调用
    componentUpdated: async function(el) {
      let blocks = el.querySelectorAll("pre code")
      for (let i = 0; i < blocks.length; i++) {
        if(!Hljs){
          Hljs = await highlight()
        }
        Hljs.highlightElement(blocks[i])
      }
    },
  })
}
