import appConfig from "./appConfig.js"
export default [
  {
    name: "A",
    meta: {
      label: "使用要素",
      icon: "idea1",
      iconType: "colorful",
      type: "folder",
    },
    component: () => import("../../views/A/A.vue"),
    children: [
      appConfig,
      {
        name: "AC2",
        meta: {
          label: "新窗口模式",
          type: "web",
        },
        component: () => import("../../views/A/Children/Children2.vue"),
      },
      {
        name: "AC3",
        meta: {
          label: "A模块子3级",
        },
        component: () => import("../../views/A/Children/Children3.vue"),
      },
    ],
  },
]
