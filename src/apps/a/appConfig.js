export default {
  name: "appConfig",
  meta: {
    label: "apps<span class='txt-blue'>配置指南</span>",
    type: "folder",
  },
  children: [
    {
      name: "deskApp",
      meta: {
        label: "<span class='txt-blue'>桌面APP</span>怎么写",
        type: "code",
        inwin: true
      },
      component: () => import("../../views/A/Children/AppConfig/DeskApp.vue"),
    },
    {
      name: "folderApp",
      meta: {
        label: "<span class='txt-blue'>打开是个文件夹</span>怎么写",
        type: "code",
        inwin: true
      },
      component: () => import("../../views/A/Children/AppConfig/FolderApp.vue"),
    },
    {
      name: "hideInDesk",
      meta: {
        label: "<span class='txt-blue'>桌面上隐藏app</span>怎么写",
        type: "code",
        inwin: true
      },
      component: () => import("../../views/A/Children/AppConfig/HideInDesk.vue"),
    },
    {
      name: "appInWin",
      meta: {
        label: "<span class='txt-blue'>app放到开始面板中</span>怎么写",
        type: "code",
        inwin: true
      },
      component: () => import("../../views/A/Children/AppConfig/AppInWin.vue"),
    },
    {
      name: "openWindow",
      meta: {
        label: "<span class='txt-blue'>打开窗口</span>调用",
        type: "code",
        inwin: true
      },
      component: () => import("../../views/A/Children/AppConfig/DispatchOpenWindow.vue"),
    },
  ],
};