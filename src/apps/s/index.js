export default [
  {
    name: "s",
    meta: {
      label: "桌面隐藏的App",
      type: "folder",
      hide: true,
      inwin: true,
    },
    children: [
      {
        name: "setting",
        meta: {
          label: "设置",
          icon: "setting",
        },
      },
    ],
  },
]
