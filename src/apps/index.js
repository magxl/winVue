/* 
app 有效可选格式 [携带type类型的图标]
参考 js/map/common/format.js
*/
import a from "./a/index.js"
import b from "./b/index.js"
import c from "./c/index.js"
import s from "./s/index.js"

export default [...a, ...b, ...c,...s];
