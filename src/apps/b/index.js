export default [
  {
    name: "B",
    meta: {
      label: "模式模拟",
      icon: "item",
      type: "folder", 
      //定义为文件夹模式folder时 component不生效
      //当children.length>0时，默认为文件夹模式
    },
    children: [
      {
        name: "BC1",
        meta: {
          label: "这是一个文件夹",
          type: "folder",
        },
        children: [
          {
            name: "BC1",
            meta: {
              label: "文件夹下的文件夹",
              type: "folder",
            },
          },
          {
            name: "BC2",
            meta: {
              label: "文件夹下一个<span class='p0-5 txt-blue'>[没定义类型]</span>的文件",
            },
            component: () => import("../../views/B/Children/Children2.vue"),
          },
          {
            name: "BC3",
            meta: {
              label: "文件夹下<span class='p0-5 txt-blue'>[定义了类型]</span>的文件",
              type: "audio",
            },
            component: () => import("../../views/B/Children/Children3.vue"),
          },
        ],
      },
      {
        name: "BC2",
        meta: {
          label: "这是一个带<span class='p0-5 txt-orange'>样式</span>的文件名"
        }
      },
      {
        name: "BC3",
        meta: {
          label: "当然也可以<span class='p0-5 txt-red fs24'>这样</span>"
        }
      },
      {
        name: "Island",
        meta: {
          label: "小岛",
          type: "video",
        },
        component: () => import("../../views/B/Children/PluginIsland.vue"),
      },
      {
        name: "Bg01",
        meta: {
          label: "背景01",
          type: "video",
        },
        component: () => import("../../views/B/Children/Bg01.vue"),
      }
    ],
  },
]
