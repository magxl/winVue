export default [
  {
    name: "C",
    meta: {
      label: "C模块",
      icon: "audio",
    },
    attrs: {
      status: "full",
    },
    component: () => import("../../views/C/C.vue"),
  },
]
