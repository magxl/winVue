# winVue
```
这是一个桌面式的拥有严格类型的可轻松又嗨皮扩展的且灰常容易理解的组件查看工具
我吹牛的 其实就是一个山寨得不太行的window
```
看[demo](http://win.floydzero.com/)
## 安装 Project setup
```
yarn install
```

### 编译 Compiles and hot-reloads for development
```
yarn serve
```

### 构建 Compiles and minifies for production
```
yarn build
```

### 修复 Lints and fixes files
```
yarn lint
```

### 说明 Intro
```
加了 文件夹内的缩略图览模式  20220209
加了 apps内配置桌面隐藏的应用，且可以放到winPanl内 20220208
加了 右下角时间 20220207
上班了 20220207
躺平
放假了 20220127
apps内可通过初始化attrs.status来设置首次打开时的窗口状态 20220126
firefox backdrop，新版本96.0.3 (64 位)失效，特此记录 20220125
win面板，进出动画调节 20220124
apps标题显示模式支持htmlelement 20220123
窗口状态切换抽离：
  diy模式->diyWindow,
  full模式->fullWindow,
  mini模式->hideWindow,
  mini到恢复->showWindow,取mini前的状态进行恢复
  关闭->closeWindow,
  所有状态都传入app来进行计算
  20220122
CodeArea格式化样式调整，加入左侧行对照 20220121
窗口full时，禁止drag 20220120
加了一个 tabbar鼠标进出时预览：
  对非激活窗口的小窗预览
  对非激活diy|full窗口的激活预览
  激活窗口不做操作
  20220119
改变了切换窗口状态的存储方式 20220118
apps载入模式变更 20220117
窗口切换时的错误修复 20220116
加了一个 iconfont单色图标 20220115
加了一个 winPanel 20220114 打码
加了一个 毛玻璃 20220113 打码
修了一下 窗口切换的逻辑 20220112 打码
还没弄好 20220111 打码
```